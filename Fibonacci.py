def serieFibonacci(num):
    first = 0
    last = 1
    for k in range(num):
        third = last+first
        first = last
        last = third
    return first

def panDigital(num):
	suma = ""
	response=False
	compare=[1,2,3,4,5,6,7,8,9]
	for digito in num:
		suma+= digito+","
	suma = suma[0:len(suma)-1]
	listed = list(suma.split(","))
	listed = [int(i) for i in listed]
	listed.sort() 
	if compare.__eq__(listed):
		response=True 
	return response

def verify():
	num=2749
	repetir_bucle = True
	while repetir_bucle :
		num = num + 1
		string=str(serieFibonacci(num))
		length=len(string)
		first = panDigital(string[0:9])
		last  = panDigital(string[length-9:length])
		if (last == True and first == True):
			break
	return num

print("Enter")
print(verify())
print("Finished")